text = require 'text'

local commandLineClass = 'command-line'
local prefix = "language-"
local linkableClass = "linkable-line-numbers"

local excludedTags = pandoc.List({ 'concepts', 'include', 'includes' })

function CheckClass(c)
  if #c.classes > 0 then
    local lang = text.lower(c.classes[1])

    if excludedTags:includes(lang) then
      return c
    end

    if not lang:match(prefix .. '.*') then
      local class = prefix .. lang
      if not c.classes:includes(class) then
        c.classes:insert(class)
      end
    end
    if lang:match('diff-.*') then
      c.classes:insert('diff-highlight')
    end
  end

  if c.attributes['user'] or c.attributes['host'] or c.attributes['prompt'] then
    c.classes:insert('command-line')
    c.classes:insert('no-line-numbers')
  end

  if c.attributes['line'] and not c.classes:includes(linkableClass) then
    c.classes:insert(linkableClass)
  end
  return c
end

if FORMAT:match 'html' then
  return {
    { Code = CheckClass, CodeBlock = CheckClass },
  }
end
