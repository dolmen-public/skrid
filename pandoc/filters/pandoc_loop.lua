local mustache = require 'modules/mustache'
local log = require 'modules/log'
local List = require 'pandoc.List'

function replaceKeyInString(str, key, metaRoute)
  if not str then
    return str
  end
  return str:gsub(key, metaRoute)
end

function loop(div)
  local metadataPath = div.attributes['for']
  if not metadataPath then
    return div
  end

  local reference = div.attributes['as']
  if not reference then
    log.markAsError(div, log.error("Missing 'as' attribute"))
    return div
  end

  if reference:sub(1, 1) ~= "$" then
    log.markAsError(div, log.error("'" .. reference .. "' must begin by '$'"))
    return div
  end

  local metadataValue = mustache.getValueWithDotNotation(metadataPath)
  if not metadataValue then
    log.markAsError(div, log.error("Metadata not found : " .. metadataPath))
    return div
  end

  local blocks = List:new()
  local metadataKeys = { }
  for k in pairs(metadataValue) do
    table.insert(metadataKeys, k)
  end
  table.sort(metadataKeys)

  for idx, key in pairs(metadataKeys) do
    local metadataFullPath = metadataPath .. '.' .. key

    blocks:extend(pandoc.walk_block(
      div,
      {
        Code = function(code)
          code.text = replaceKeyInString(code.text, reference, metadataFullPath)
          return code
        end,
        Link = function(link)
          link.target = replaceKeyInString(link.target, reference, metadataFullPath)
          return link
        end,
        Div = function(d)
          if d.attributes['if'] then
            d.attributes['if'] = replaceKeyInString(d.attributes['if'], reference, metadataFullPath)
          end
          return loop(d)
        end,
        Span = function(d)
          if d.attributes['if'] then
            d.attributes['if'] = replaceKeyInString(d.attributes['if'], reference, metadataFullPath)
          end
          return loop(d)
        end
      }
    ).content)
  end

  return blocks
end

return {
  { Meta = mustache.get_vars },
  { Div = loop }
}
