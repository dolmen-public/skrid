require 'modules/tests'

local hasTable = false
local hasFigure = false
local enable = false

function get_vars (meta)
  if meta['autoListOf'] then
    enable = true
  end
end

function Table (table)
  hasTable = true
end

function Image (i)
  hasFigure = true
end

function Pandoc (doc)
  if enable then
    if hasTable then
      doc.blocks:insert(pandoc.Plain(pandoc.RawInline('latex', '\\listoftables')))
    end

    if hasFigure then
      doc.blocks:insert(pandoc.Plain(pandoc.RawInline('latex', '\\listoffigures')))
    end
    return doc
  end
end

if FORMAT:match 'latex' then
  return {
    { Meta = get_vars },
    { Table = Table, Image = Image },
    { Pandoc = Pandoc }
  }
end
