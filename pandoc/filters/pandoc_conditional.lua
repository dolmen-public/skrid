local conditional = require 'modules/conditional'
local mustache = require 'modules/mustache'

return {
  { Meta = mustache.get_vars },
  { Div = conditional.apply },
  { Span = conditional.apply }
}


