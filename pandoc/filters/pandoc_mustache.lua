local mustache = require 'modules/mustache'
local concept = require 'modules/concept'
local log = require 'modules/log'

-- Replace an element if matched
--
function replaceCode (code)
  -- Only process non concept
  if not concept.isReference(code) then
    return mustache.replaceMatchingAny(code.text, code)
  end
end

function replaceLink (link)
  if re.find(link.target, "('%7B%7B' / '{{')") then
    local anchor = re.gsub(link.target, "('%20' / %s)*('%60' / '`')?('%7B%7B' / '{{')('%20' / %s)*", "")
    anchor = re.gsub(anchor, "('%20' / %s)*('%7D%7D' / '}}')('%60' / '`')?('%20' / %s)*", "")

    local metadataValue = mustache.getValueWithDotNotation(anchor)
    if metadataValue then
      link.target = metadataValue
    else
      log.markAsError(link, log.warn("Metadata not found : " .. anchor .. " | Skipping"))
    end
  end
  return link
end

return {
  { Meta = mustache.get_vars },
  { Code = replaceCode },
  { Link = replaceLink }
}
