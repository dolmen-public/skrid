-- Add latex environment on specific div
require 'modules/tests'

local configuration = {}

function get_vars(meta)
  if meta['pandoc-latex-environment'] then
    configuration = meta['pandoc-latex-environment']
  end
end

function transclude(div)
  local environment = null

  for k, v in pairs(div.classes) do
    if configuration[v] then
      if type(configuration[v]) == 'table' then
        environment = table.unpack(configuration[v])['text']
      else
        environment = configuration[v]
      end
      break
    end
  end

  if environment then
    return {
      pandoc.Plain(pandoc.RawInline('latex', '\\begin{' .. environment .. '}')),
      div,
      pandoc.Plain(pandoc.RawInline('latex', '\\end{' .. environment .. '}'))
    }
  end
  return div
end

if FORMAT:match 'latex' then
  return {
    { Meta = get_vars },
    { Div = transclude }
  }
end
