-- keep div with (.*)-only class only if $1-visible variable is true

require 'modules/tests'

local vars = {}

function get_vars (meta)
  for key, val in pairs(meta) do
    local type = key:match('onlydiv%-(%w+)')
    if type then
      vars[type] = not not val
    end
  end
end

function Div (b)
  if b.classes then
    for key, value in pairs(b.classes) do
      local type = value:match('(%w+)%-only')
      if type then
        if vars[type] then
          return b
        else
          return {}
        end
      end
    end
  end
end

return {
  { Meta = get_vars },
  { Div = Div },
}
