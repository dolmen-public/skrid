#!/usr/bin/env bash
set -e
set -u

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

wget -q -O "${DIR}/pandoc-quotes.lua" \
  https://raw.githubusercontent.com/pandoc/lua-filters/master/pandoc-quotes.lua/pandoc-quotes.lua
