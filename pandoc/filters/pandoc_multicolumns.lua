-- Inject multicolumns command for latex

require 'modules/tests'

function Div(d)
  if inTable(d.classes, "multicolumns") then
    nb = 0
    for key, child in pairs(d.content) do
      nb = nb + 1
      if child.tag == 'Div' then
        if inTable(child.classes, 'column') then
        else
          child.classes:insert('column')
        end
      else
        d.content[key] = pandoc.Div({ child })
        d.content[key].classes:insert('column')
      end
      if FORMAT:match 'latex' then
        d.content[key].content:insert(pandoc.Plain(pandoc.RawInline('latex', '\\columnbreak')))
      end
    end

    if FORMAT:match 'latex' then
      return {
        pandoc.Plain(pandoc.RawInline('latex', '\\begin{multicols}{' .. nb .. '}')),
        d,
        pandoc.Plain(pandoc.RawInline('latex', '\\end{multicols}'))
      }
    end
    return d
  end
end
