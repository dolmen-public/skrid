require 'modules/tests'

local mustache = require 'modules/mustache'
local Concept = require 'modules/concept'
local log = require 'modules/log'
local text = require 'text'

local formatConceptInHeader = false
local formatConceptClass = 'formatConcept'

-- Parse metadata
--
function get_vars (meta)
  if meta['formatConceptInHeader'] then
    formatConceptInHeader = true
  end
  mustache.get_vars(meta)
end

-- Reference a concept
--
function ConceptRef(c)
  if Concept.isReference(c) then
    local alias_for = c.attributes['alias-for']
    local concept = Concept.add(c.identifier, c.text, alias_for)

    if alias_for then
      return {}
    end

    if FORMAT:match 'html' then
      return pandoc.Span(concept.value, { id = c.identifier, class = Concept.CLASS })
    elseif FORMAT:match 'markdown' then
      if not c.classes:includes(Concept.CLASS) then
        c.classes:insert(Concept.CLASS)
      end
      c.text = concept.value
      return c
    else
      return pandoc.Span(pandoc.Underline(concept.value), { id = c.identifier, class = Concept.CLASS })
    end
  end
end

-- Parse Header
--
function HeaderRef(h)
  if not isEmpty(h.identifier) and h.tag == 'Header' then

    -- Replace concept inside header
    h = pandoc.walk_block(h,
      {
        Code = function(code)
          return ReplaceByConceptLink(code, formatConceptInHeader or h.classes:includes(formatConceptClass))
        end }
    )
    Concept.add(h.identifier, h.content, nil)
    return h
  end
end

-- Process link to concept
--
function ReplaceByConceptLink(c, format)
  local anchor = c.text:match('^::(.*)')
  if not anchor then
    return
  end

  local modifier = string.match(anchor, '^.-%s*|%s*(.-)%s*$')
  if modifier then
    anchor = string.match(anchor, '^(.-)%s*|%s*.-%s*$')
  end

  local concept = Concept.list[anchor]
  if concept then
    return Concept.toLink(concept, modifier, format)
  end

  log.markAsError(c, log.error("Missing concept : [" .. anchor .. "]"))
  return c
end

-- Creates list of concepts
--
function ConceptList(c)
  if not c.classes:includes(text.lower('concepts')) then
    return
  end

  local list = pandoc.List()
  local found = false

  for pattern in string.gmatch(c.text, "[^\n]+") do
    local sublist = pandoc.List()
    for key, concept in pairs(Concept.list) do
      if key:match('^' .. pattern .. '$') then
        found = true
        sublist:insert(Concept.toLink(concept, nil, true))
      end
    end
    sublist:sort(function(a, b)
      return pandoc.utils.stringify(b.content) > pandoc.utils.stringify(a.content)
    end)
    list:extend(sublist)
  end

  if found then
    return pandoc.BulletList(list:map(function(li)
      return pandoc.Plain(li)
    end))
  end

  return log.markAsError(nil, log.error("Missing concept : [" .. c.text .. "]"))
end


-- Creates definitions block
--
local DefClass = "ConceptDef"
function ConceptDefinition(def)
  local key = def.attributes["def-for"]
  local error
  if key then
    local concept = Concept.list[key]
    if concept then
      if concept.definition then
        return log.markAsError(nil, log.warn("Definition already exist for concept : [" .. key .. "], Skipping"))
      else
        concept.definition = true
      end
    else
      error = log.error("Definition refer to a missing concept : [" .. key .. "]")
    end

    if (FORMAT:match 'html' or FORMAT:match 'markdown') and not def.classes:includes(DefClass) then
      def.classes:insert(DefClass)
      log.markAsError(def, error)
      return def
    end

    if FORMAT:match 'html' then
      local span =pandoc.Span(c.text, { id = c.identifier, class = Concept.CLASS })
      log.markAsError(def, error)
      return span
    end
  end
end

return {
  { Meta = get_vars },
  { Code = ConceptRef },
  { Header = HeaderRef },
  { Div = ConceptDefinition },
  { Code = function(code)
    return ReplaceByConceptLink(code, true)
  end,
    CodeBlock = ConceptList
  },
}
