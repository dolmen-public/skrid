local mustache = require 'modules/mustache'
local String = require 'modules/String'
local log = require 'modules/log'

local Concept = {}

Concept.CLASS = "Concept"

Concept.list = {}

-- Add a concept to the buffer list
--
function Concept.add(key, value, alias_for)
  if Concept.list[key] then
    log.warn("Duplicate concept : [" .. key .. "] with value [" .. pandoc.utils.stringify(value) .. "]")
  else
    Concept.list[key] = {
      value = pandoc.utils.stringify(mustache.replaceMatchingAny(pandoc.utils.stringify(value), value)),
      anchor = (alias_for or key)
    }
  end

  return Concept.list[key]
end

-- Transform a concept to a link
--
function Concept.toLink(concept, modifier, format)
  if format then
    local link = pandoc.Link(
      String.filter(modifier, concept.value),
      '#' .. concept.anchor,
      "",
      { class = Concept.CLASS .. "Link" }
    )
    link.attributes.concept = concept.anchor
    return link
  else
    return pandoc.Span(String.filter(modifier, concept.value))
  end
end

function Concept.isReference(code)
  return not isEmpty(code.identifier) and (code.classes:includes(Concept.CLASS) or #code.classes == 0)
end

return Concept
