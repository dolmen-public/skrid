text = require 'text'
tests = require 'modules/tests'
pandoc.utils = require 'pandoc.utils'
local createCommand = require 'modules/createCommand'
local log = require 'modules/log'

local conditional = {}

function conditional.eval(command)
  if (command.functionType == createCommand.functionTypes.TRUTHY) then
    return command.actual and string.len(command.actual) > 0
  end

  if command.functionType == createCommand.functionTypes.IS then
    return (command.actual == command.expected) ~= (command.negate)
  end

  return true
end

-- Conditional filter
function conditional.apply (b)
  if b.attributes['if'] then
    command = createCommand.getAttributes(b.attributes['if'])
    if next(command.errors) then
      local error = log.error(command.errors)
      log.markAsError(b, error)
      return b
    end

    if not conditional.eval(command) then
        return {}
    end
  end

  return b
end

return conditional
