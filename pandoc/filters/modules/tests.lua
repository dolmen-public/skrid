function isEmpty(s)
  return s == nil or s == ''
end

function inTable(tbl, item)
  if tbl then
    for key, value in pairs(tbl) do
      if value == item then
        return key
      end
    end
  end
  return false
end

function dump(o)
  if type(o) == 'table' then
    local s = pandoc.List({ '{' })
    for k, v in pairs(o) do
      if type(k) ~= 'number' then
        k = '"' .. k .. '"'
      end
      s:insert('[' .. k .. '] = ' .. dump(v) .. ',')
    end
    s:insert(#s + 1, '} ')
    return table.concat(s, '')
  else
    return tostring(o)
  end
end
