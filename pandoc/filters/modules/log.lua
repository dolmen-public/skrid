json = require "modules/json"
ERROR_CLASS = "PandocError"

errorIdx = 0
local log = {}

function write(message, severity)
  errorIdx = errorIdx+1
  local error = {
    severity = severity,
    id = 'error'.. FORMAT ..errorIdx,
    message = message
  }

  if type(message) == 'table' then
    error.message = ''
    for key, val in pairs(message) do
      error.message = error.message .. val .. '\n'
    end
  end

  io.stderr:write(json.encode(error) .. "\n")
  return error
end

function log.warn(message)
  return write(message, 'WARN')
end

function log.error(message)
  return write(message, 'ERROR')
end

function log.markAsError(element, error)
  if (not error) then
    return
  end
  -- only if target element support attributes
  if not element then
    element = pandoc.Span("Error")
  end

  if (element and element.attributes) then
    element.attributes["error-id"] = error.id
    element.attributes["error-severity"] = error.severity
    element.attributes["error-message"] = error.message
    element.classes:insert(ERROR_CLASS)
  end

  return element
end

return log
