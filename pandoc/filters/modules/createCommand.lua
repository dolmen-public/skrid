text = require 'text'
tests = require 'modules/tests'
pandoc.utils = require 'pandoc.utils'
local mustache = require 'modules/mustache'
local commandChecker = require 'modules/commandChecker'

local createCommand = {}

createCommand.functionTypes = {
  IS = 'is',
  IN = 'in',
  TRUTHY = 'truthy'
}

local CommandWordNegate = 'not'

function getActual(pointer, command)
  local variable = getNextVariables(pointer)
  command.actual = mustache.getValueWithDotNotation(variable)
  table.insert(command.errors, commandChecker.isActualOk(variable, command.actual))
  return variable
end

-- Returns CommandWordFunction if starts with one
function getFirstCommandWord(str, command)
  for key, val in pairs(createCommand.functionTypes) do
    if (string.sub(str, 1, string.len(val)) == val) then
      return val
    end
  end
end

function getRawValue(pointer)
  if (string.sub(pointer, 1, 1) == "'") then
    for match in string.gmatch(pointer, "'([^']+)'") do
      return match
    end
  end
end

function getFunctionType(pointer, command)
  local commandWord = getFirstCommandWord(pointer, command)
  if (commandWord) then
    command.functionType = commandWord
    return commandWord
  end
end

function getNegation(pointer, command)
  if string.sub(pointer, 1, string.len(CommandWordNegate)) == CommandWordNegate then
    command.negate = true
    return CommandWordNegate
  end
end

function getNextVariables(pointer)
  for match in string.gmatch(pointer, "([^%s]+)") do
    return match
  end
end

function getExpected(pointer, command)
  local variable
  if pointer then
    command.expected = getRawValue(pointer)
    if not command.expected then
      variable = getNextVariables(pointer)
      command.expected = mustache.getValueWithDotNotation(variable)
    end
  else
    table.insert(command.errors, "Missing last argument is empty.")
  end

  table.insert(command.errors, commandChecker.isExpectedOk(variable, command.expected))
end

function advance(str, str2)
  if not str2 then
    return str
  end

  local nbChar = string.len(str2)

  if string.len(str) <= nbChar then
    return nil
  end

  return (string.sub(str, -(string.len(str) - nbChar))):match "^%s*(.-)%s*$"
end

-- Returns command from given string
function createCommand.getAttributes(str)
  command = {};
  command.functionType = nil
  command.negate = false
  command.errors = {}

  if string.len(str) == 0 then
    table.insert(command.errors, "missing arguments")
  else
    local pointer = str
    pointer = advance(pointer, getActual(pointer, command))
    if pointer and string.len(pointer) > 0 then
      pointer = advance(pointer, getNegation(pointer, command))
      pointer = advance(pointer, getFunctionType(pointer, command))
      getExpected(pointer, command)
    else
      command.functionType = createCommand.functionTypes.TRUTHY
    end

    if not command.functionType then
      table.insert(command.errors, "missing function")
    end
  end
  return command
end

return createCommand
