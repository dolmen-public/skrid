require 'modules/tests'
local log = require 'modules/log'

local commandChecker = {}


function commandChecker.isActualOk(keyword, data)
  if keyword == "not" or keyword == "is" then
    message = "missing first argument"
  elseif data == nil then
    message = "Keyword error : [" .. keyword .. "] doesn't exist in metadata."
  elseif type(data) == "table" then
    message = "The first argument is incomplete : [" .. keyword .. "]."
  else
    return nil
  end
  return message
end

function commandChecker.isExpectedOk(keyword, data)
  if keyword == "not" then
    message = "the \"not\" negation must be before the \"is\" condition."
  elseif keyword and data == "" then
    message = "Data field is empty in yaml file : [" .. keyword .. "]."
  elseif keyword and data == nil then
    message = "Syntax Error : [" .. keyword .. "] doesn't exist in metadata."
  else
    return nil
  end
  return message
end

return commandChecker
