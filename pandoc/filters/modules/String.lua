text = require 'text'
local log = require 'modules/log'

local String = {}

function String.filter(filter, str)
  if not filter then
    return str
  end

  if filter == 'lower' then
    return text.lower(str)
  end

  if filter == 'upper' then
    return text.upper(str)
  end

  if filter == 'capital' then
    return tostring(str:gsub("^%l", text.upper))
  end

  log.markAsError(str, log.warn("Unknown string filter : [" .. filter .. "]"))
  return str
end

return String
