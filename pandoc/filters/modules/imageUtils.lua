local path = require 'pandoc.path'

-- modules
local log = require 'modules/log'

local imageUtils = {}

function imageUtils.normalizePath(image, currentDirectory, baseDirectory)
  -- If image paths are relative then prepend include file path
  if image.src and not image.src:match('://') and path.is_relative(image.src) then
    -- prepend with current directory
    local imagePath = path.normalize(path.join({ currentDirectory, image.src }))
    local fh = io.open(imagePath)

    if fh == nil then
      -- prepend with base directory
      imagePath = path.normalize(path.join({ baseDirectory, image.src }))
      fh = io.open(imagePath)

      if fh == nil then
        local errorMarker = pandoc.Span(image.caption)
        log.markAsError(errorMarker, log.error('Image not found : ' .. image.src))
        return errorMarker
      end
    end

    image.src = imagePath
    io.close(fh)
  end
  return image
end

return imageUtils
