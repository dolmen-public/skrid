local log = require 'modules/log'
require 'modules/tests'

local mustache = {}
local vars = {}

function mustache.get_vars (meta)
  vars = meta
end

function mustache.getValueWithDotNotation(strDotNotation)
  local pointer = vars
  for token in string.gmatch(strDotNotation, "[^.]+") do
    if not pointer[token] then
      return nil
    end

    pointer = pointer[token]
  end
  return pointer
end

function mustache.replaceMatchingAny(str, element)
  if str then
    local anchor = str:match('%s*{{%s*(.-)%s*}}*s*')
    if anchor then

      local metadataValue = mustache.getValueWithDotNotation(anchor)
      if metadataValue then
        local blocks = pandoc.read(metadataValue).blocks
        return pandoc.utils.blocks_to_inlines(blocks)
      else
        log.markAsError(element, log.warn("Metadata not found : " .. anchor .. " | Skipping"))
      end
    end
  end
  return element
end

return mustache
