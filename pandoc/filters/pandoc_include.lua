--- include-files.lua – filter to include Markdown files
---
--- Copyright: © 2019–2021 Albert Krewinkel
--- License:   MIT – see LICENSE file for details

-- Module pandoc.path is required and was added in version 2.12
PANDOC_VERSION:must_be_at_least '2.12'

local List = require 'pandoc.List'
local path = require 'pandoc.path'
local system = require 'pandoc.system'
local lfs = require "lfs"

-- modules
local log = require 'modules/log'
local conditional = require 'modules/conditional'
local mustache = require 'modules/mustache'
local imageUtils = require 'modules/imageUtils'

--- Get include auto mode
local include_auto = false
local files_in_meta = {}

local baseDirectory = system.get_working_directory()
local currentDirectory = path.normalize(path.join(
  {
    system.get_working_directory(),
    path.directory(PANDOC_STATE.input_files[1])
  }
))

function get_vars (meta)
  mustache.get_vars(meta)
  if meta['include-auto'] then
    include_auto = true
  end
  if meta['includes'] then
    files_in_meta = meta['includes']
  end
end

--- Keep last heading level found
local last_heading_level = 0
function update_last_level(header)
  last_heading_level = header.level
end

--- Update contents of included file
local function update_contents(blocks, shift_by, include_path)
  local update_contents_filter = {
    -- Shift headings in block list by given number
    Header = function(header)
      if shift_by then
        header.level = header.level + shift_by
      end
      return header
    end,

    Image = function(image)
      return imageUtils.normalizePath(image, include_path, baseDirectory)
    end,
    -- Update path for include-code-files.lua filter style CodeBlocks
    CodeBlock = function(cb)
      if cb.attributes.include and path.is_relative(cb.attributes.include) then
        cb.attributes.include = path.normalize(path.join({ include_path, cb.attributes.include }))
      end
      return cb
    end
  }

  return pandoc.walk_block(pandoc.Div(blocks), update_contents_filter).content
end

--- Filter function for code blocks
function transclude (cb)
  cb = conditional.apply(cb)

  -- ignore code blocks which are not of class "include".
  if not (cb.classes and (cb.classes:includes 'include' or cb.classes:includes 'includes')) then
    return cb
  end

  -- Markdown is used if this is nil.
  local format = cb.attributes['format']

  -- Attributes shift headings
  local shift_heading_level_by = 0
  local shift_input = cb.attributes['shift-heading-level-by']
  if shift_input then
    shift_heading_level_by = tonumber(shift_input)
  else
    if include_auto then
      -- Auto shift headings
      shift_heading_level_by = last_heading_level
    end
  end

  --- keep tracks before recursion
  local buffer_last_heading_level = last_heading_level
  local buffer_current_directory = currentDirectory

  local blocks = List:new()
  for line in cb.text:gmatch('[^\n]+') do
    if line:sub(1, 2) ~= '//' then
      local key = line:match('{{(.+)}}')
      if key then
        if files_in_meta[key] then
          line = files_in_meta[key]
        else
          local error = log.warn("Unknown key {{" .. key .. "}}")
          blocks:insert(log.markAsError(nil, error))
        end
      end


      -- opening file from current path
      local fromCurrent = path.normalize(path.join({ currentDirectory, line }))
      -- opening file from base path
      local fromBase = path.normalize(path.join({ baseDirectory, line }))

      local currentPath = fromCurrent
      local fh = io.open(currentPath)
      if not fh then
        currentPath = fromBase
        fh = io.open(currentPath)
      end

      if not fh then
        blocks:insert(log.markAsError(nil, log.warn("Cannot include file " .. line .. ' from ' .. fromCurrent .. ' or ' .. fromBase)))
      else
         if lfs.attributes(currentPath, "mode") == "directory" then
           -- File found but is a directory, forward to main
           currentPath = path.normalize(path.join({ currentPath, 'main.md' }))
           fh = io.open(currentPath)

           if not fh then
             blocks:insert(log.markAsError(nil, log.warn('Cannot include directory ' .. line .. ': Missing file main.md')))
           end
         end
      end

      if fh then
        -- read file as the given format with global reader options
        local contents = pandoc.read(
          fh:read '*a',
          format,
          PANDOC_READER_OPTIONS
        ).blocks

        currentDirectory = path.directory(currentPath)

        last_heading_level = 0
        -- recursive transclusion
        contents = system.with_working_directory(
          currentDirectory,
          function()
            return pandoc.walk_block(
              pandoc.Div(contents),
              { Header = update_last_level, CodeBlock = transclude }
            )
          end).content
        --- reset to level before recursion
        last_heading_level = buffer_last_heading_level
        blocks:extend(update_contents(contents, shift_heading_level_by,
          currentDirectory))
        fh:close()

        --- reset to current directory before recursion
        currentDirectory = buffer_current_directory
      end
    end
  end
  return blocks
end

return {
  { Meta = get_vars },
  --- normalize image on entry file
  { Image = function(image)
    return imageUtils.normalizePath(image, currentDirectory, baseDirectory)
  end },
  { Header = update_last_level, CodeBlock = transclude }
}
