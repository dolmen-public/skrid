require 'modules/tests'
local log = require 'modules/log'

local identifiers = {}

function Block (b)
  if not isEmpty(b.identifier) then
    identifiers[b.identifier] = true
  end
end

function Inline (i)
  if not isEmpty(i.identifier) then
    identifiers[i.identifier] = true
  end
end

function Link (l)
  local anchor = l.target:match('^[^http]?#(.*)')
  if anchor and not identifiers[anchor] then
    return log.markAsError(l, log.warn("Broken link: " .. anchor))
  end
end

return {
  { Block = Block, Inline = Inline, Span = Inline },
  { Link = Link }
}
