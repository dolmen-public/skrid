-- Transform linkToFile.md#anchor to #anchor

function Link(el)
  local anchor = el.target:match("^[^(http)]+.*md(#.*)$")
  if anchor then
    el.target = anchor
  end
  return el
end
