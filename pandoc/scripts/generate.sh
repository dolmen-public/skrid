#!/usr/bin/env bash

# ${1} is temporary yaml configuration
# ${2} is target extension
shopt -s globstar nullglob

source /.pandoc/scripts/check.sh

outputFile="$(yq r "${1}" 'output-file')"
outputFile=${outputFile:-"output"}
logFile="/public/${outputFile}_${2}_log.json"
rm -f "${logFile}"
yq w -i "${1}" "variables.logfile" "${logFile}"

if [[ "${2}" == 'html' ]]; then
  yq w -i "${1}" "output-file" "/public/index.html"
  yq w -i "${1}" "variables.pdfFilename" "/public/${outputFile}.pdf"
  if [[ "${MULTIPLE_TARGET}" != false ]]; then
    echo "Generate | ${CURRENT_DOC}"
    yq w -i "${1}" "variables.currentDoc" "${CURRENT_DOC}"

    shopt -s globstar nullglob
    for file in /data/**/pandoc.yml
    do
      yq w -i "${1}" "variables.availableDocs[+]" "$(realpath --relative-to="/data" "$(dirname "$file")")"
    done
  fi
else
  yq w -i "${1}" "output-file" "/public/${outputFile}.${2}"
fi

# For everything appart markdown, inject intermediate markdown format as source
if [[ "${2}" != 'md' ]]; then
  yq w -i "${1}" "input-file" "/public/${outputFile}.md"
  cp "/public/${outputFile}_md_log.json" "${logFile}"
elif [[ "${MULTIPLE_TARGET}" != false ]]; then
  yq w -i "${1}" "input-file" "${CURRENT_DOC}/main.md"
fi

while read -r line
do
  echo '{"id":"","message":"'"${line}"'","severity":"ERROR"}' >> "${logFile}"
done < "${3}"

echo "Generate | $(yq r "${1}" 'output-file') with ${1}"
INJECT_DATE=${SPI_DATE:-$(/.pandoc/scripts/date.sh "${1}")}
pandoc -V date="${INJECT_DATE}" -M date="${INJECT_DATE}" -d "${1}" 2> >(tee -a "${logFile}")
