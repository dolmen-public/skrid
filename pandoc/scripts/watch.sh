#!/usr/bin/env bash
set -e
set -u

WATCHER_CONFIG="${SPI_TMP_DIR}/watcher.yml"

arguments=("${PANDOC_DATA}/defaults/watcher.yml")

shopt -s nullglob
for file in "${CONFIG_PATH}/"*watcher.yml; do
    arguments+=("${file}")
done

if [[ -f "/data/watcher.yaml" ]]; then
  arguments+=("/data/watcher.yaml")
elif [[ -f "/data/watcher.yml" ]]; then
  arguments+=("/data/watcher.yml")
fi

yq merge -x -a=append "${arguments[@]}" >"${WATCHER_CONFIG}"

function getConfig() {
  yq read "${WATCHER_CONFIG}" "$1"
}

directories="$(getConfig 'directories.*')"

WATCHER_DELAY=$(getConfig "delay")
export WATCHER_DELAY

# shellcheck disable=SC2153
function watch() {
  echo "Watcher | Listening to change on ${directories}, with ${WATCHER_CONFIG}"

  # shellcheck disable=SC2086
  inotifywait -mr -e "$(getConfig events)" --exclude "$(getConfig exclude)" ${directories} |
    while read -r notifies; do
      echo "${notifies}" >>"${NOTIFICATIONS}"
    done
}

function servePublic() {
  #Docs https://github.com/joewalnes/websocketd/wiki
  websocketd --port=8080 \
    --loglevel="$(getConfig "server.log")" \
    --staticdir=/public "${PANDOC_DATA}/scripts/onMessage.sh"
  echo "Watcher | Live server started"
}

servePublic &
watch &
