#!/usr/bin/env bash
set -e
set -u

IFS=" " read -r -a arguments <<< "$(/.pandoc/scripts/datafile.sh "${1}" "${3}")"

yq merge -x -a=append "${arguments[@]}" >"${2}"
