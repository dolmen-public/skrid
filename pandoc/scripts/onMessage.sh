#!/bin/bash

notifyChange() {
inotifywait \
    -mr "/public" \
    -e "create,delete,modify,move"
}

notifyChange &

while read C
do
  echo "${C}" > "/multiple_target/current"
  cat "/multiple_target/current" 1>/dev/null
done
exit
