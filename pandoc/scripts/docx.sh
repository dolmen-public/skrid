#!/usr/bin/env bash
set -e
set -u
EXTENSION=docx

if [ "${SPI_FORMAT}" != all ] && [ "${SPI_FORMAT}" != ${EXTENSION} ]; then
    exit 0
fi

TMP_DEFAULTS="${SPI_TMP_DIR}/pandoc.${EXTENSION}.yml"
TMP_ERROR_FILE="/tmp/launcherError"
cat /dev/null > "${TMP_ERROR_FILE}"

/.pandoc/scripts/generateFullDatafile.sh "${EXTENSION}" "${TMP_DEFAULTS}" "${TMP_ERROR_FILE}"
/.pandoc/scripts/generate.sh "${TMP_DEFAULTS}" "${EXTENSION}" "${TMP_ERROR_FILE}"
