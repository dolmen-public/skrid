#!/usr/bin/env bash
shopt -s nullglob

source /.pandoc/scripts/check.sh

arguments=("${PANDOC_DATA}/defaults/all.yml" "${PANDOC_DATA}/defaults/${1}.yml")

ERROR_FILE="${2:-/dev/null}"

# Check if yaml is valid
function validateYaml {
  if ERROR="$(yq r "${1}" 2>&1 1>/dev/null)" ; then
    arguments+=("${1}")
  else
    echo "Ignoring invalid file ${1} : ${ERROR}" | tee -a "${ERROR_FILE}" >&2
  fi
}

# Main
for file in "${CONFIG_PATH}/"*.all.yml; do
    validateYaml "${file}"
done

for file in "${CONFIG_PATH}/"*."${1}".yml; do
    validateYaml "${file}"
done

validateYaml "${PANDOC_PATH}"

for file in "${TARGET_FOLDER}/"*.all.yml; do
    validateYaml "${file}"
done

for file in "${TARGET_FOLDER}/"*."${1}".yml; do
    validateYaml "${file}"
done

if [[ "${MULTIPLE_TARGET}" != false ]]; then
  for file in "/data/"*.all.yml; do
      validateYaml "${file}"
  done

  for file in "/data/"*."${1}".yml; do
      validateYaml "${file}"
  done
fi

echo "${arguments[@]}"
