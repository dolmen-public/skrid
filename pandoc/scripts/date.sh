#!/usr/bin/env bash
set -e
set -u

lang=$(yq read "$1" 'variables.lang')
LOCALE=$(find /usr/share/i18n/locales/musl/ -name "*${lang}*" -exec basename {} \; | head -1)
LANG=${LOCALE:-en_US.UTF-8}
date "${SPI_DATE_ARG}"
