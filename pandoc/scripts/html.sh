#!/usr/bin/env bash
set -e
set -u
EXTENSION=html

if [ "${SPI_FORMAT}" != all ] && [ "${SPI_FORMAT}" != ${EXTENSION} ]; then
    exit 0
fi

if [ -d "${REPORT_FILES}" ]; then
    rsync -rq "${REPORT_FILES}" /public/ || true
fi

rsync -q "${CONFIG_PATH}"/favicon.png /public/

TMP_DEFAULTS="${SPI_TMP_DIR}/pandoc.${EXTENSION}.yml"
TMP_ERROR_FILE="/tmp/launcherError"
cat /dev/null > "${TMP_ERROR_FILE}"
/.pandoc/scripts/generateFullDatafile.sh "${EXTENSION}" "${TMP_DEFAULTS}" "${TMP_ERROR_FILE}"

if [ "${SPI_WATCH}" = true ]; then
  yq merge -x -a=append -i "${TMP_DEFAULTS}" "${PANDOC_DATA}/defaults/reload.html.yml"
fi

/.pandoc/scripts/generate.sh "${TMP_DEFAULTS}" "${EXTENSION}" "${TMP_ERROR_FILE}"

sed -i 's#\"/public/media/#\"media/#g' /public/index.html

if [ "${SPI_WATCH}" != true ]; then
    rm -f "/public/reload.js" "/public/reload.css"
fi
