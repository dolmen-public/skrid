#!/usr/bin/env bash
set -e
set -u
shopt -s nullglob


## Mirroring /data folder with symlinks
rm -rf "${WORKDIR:?}/"*
cp -asf /data/* "${WORKDIR}"

for script in *.before.sh; do
  echo "Run | Run ${script}"
  bash "${script}"
done

echo "Run | Launching"
/.pandoc/scripts/markdown.sh
/.pandoc/scripts/html.sh
/.pandoc/scripts/docx.sh
/.pandoc/scripts/pdf.sh

for script in *.after.sh; do
  echo "Run | Run ${script}"
  bash "${script}"
done

echo "Run | Done"
