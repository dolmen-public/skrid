#!/usr/bin/env bash
set -e
set -u

TARGET_FOLDER="/data"
PANDOC_PATH="./pandoc.yml"
MULTIPLE_TARGET_CURRENT_FILE="${MULTIPLE_TARGET_CURRENT_DIR}/current"

if [[ "${MULTIPLE_TARGET}" != false ]]; then
  CURRENT_DOC=${MULTIPLE_TARGET}
  if [[ -f "${MULTIPLE_TARGET_CURRENT_FILE}" ]]; then
    USER_CURRENT_DOC=$(head -1 <"${MULTIPLE_TARGET_CURRENT_FILE}")
    if [[ -d "/data/${USER_CURRENT_DOC}" ]]; then
      CURRENT_DOC="${USER_CURRENT_DOC}"
    fi
  fi
  export CURRENT_DOC
  TARGET_FOLDER="/data/${CURRENT_DOC}"
  PANDOC_PATH="${TARGET_FOLDER}/pandoc.yml"
fi

export TARGET_FOLDER


if [[ ! -f "${PANDOC_PATH}" ]]; then
    echo "Check | Missing pandoc.yml at ${PANDOC_PATH}"
    exit 1
fi

export PANDOC_PATH
