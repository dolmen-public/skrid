#!/usr/bin/env bash
set -e
set -u
EXTENSION=md

TMP_DEFAULTS="${SPI_TMP_DIR}/pandoc.${EXTENSION}.yml"
TMP_ERROR_FILE="/tmp/launcherError"
cat /dev/null > "${TMP_ERROR_FILE}"

/.pandoc/scripts/generateFullDatafile.sh markdown "${TMP_DEFAULTS}" "${TMP_ERROR_FILE}"
/.pandoc/scripts/generate.sh "${TMP_DEFAULTS}" "${EXTENSION}" "${TMP_ERROR_FILE}"
