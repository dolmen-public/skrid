This sentence has a [link](https://foo.exemple) that has a reference This sentence has a note [^1] This sentence has an inline note [^2]

::: tip
This tip has a [link](https://bar.exemple) that has a reference This tip has a note [^3] This tip has an inline note [^4]
:::

[^1]: This is a note

[^2]: This is an inline note

[^3]: This is a note

[^4]: This is an inline note
