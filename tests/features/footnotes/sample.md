This sentence has a [link](https://foo.exemple) that has a reference
This sentence has a note [^note]
This sentence has an inline note ^[This is an inline note]

::: tip
This tip has a [link](https://bar.exemple) that has a reference
This tip has a note [^note2]
This tip has an inline note ^[This is an inline note]
:::

[^note]: This is a note
[^note2]: This is a note
