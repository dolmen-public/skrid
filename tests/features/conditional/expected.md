``` yml
metadata:
  explanation: display
  foo:
    bar: test
    bar2: test
    barSpace: te st
```

::: {if="explanation is 'display'"}
Only the blocks where the condition is fulfilled are displayed. The mandatory syntax is :

-   {**if=** keyword must be inside curly braces after the declaration of the block}.

-   "The content of condition must be inside double quotes".

-   The first argument is the name on the variable from the **metadata** with its full hierarchy.

-   The second argument has only 2 options :

    -   **is** will display the block if the other arguments match
    -   **not is** will display the block if the other arguments don't match

-   The third argument can have 2 types :

    -   'A raw value inside quotes'.
    -   The name of another **metadata** variable.
:::

Works with Div Blocks

::: {if="foo.barSpace is 'te st'"}
this condition compares the content of the **foo.barSpace** variable with the raw value **'te st'** it will be displayed if there **is** a match
:::

::: {if="foo.bar not is 'something'"}
this condition compares the content of the **foo.bar** variable with the raw value **'test'** it will be displayed if there **is not** a match
:::

::: {if="foo.bar is foo.bar2"}
this condition compares the content of the **foo.bar** variable with the **foo.bar2** variable it will be displayed if there **is** a match
:::

Works with Span Blocks

[this condition compares the content of the **foo.bar** variable with the raw value **'test'** it will be displayed if there **is** a match]{if="foo.bar is 'test'"}

[this condition compares the content of the **foo.bar** variable with the **foo.barSpace** variable it will be displayed if there **is not** a match]{if="foo.bar not is foo.barSpace"}
