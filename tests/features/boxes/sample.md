::: box
Lorem ipsum dolor ...
:::

::: success
Lorem ipsum dolor ...
:::

::: warn
Lorem ipsum dolor ...
:::

::: error
Lorem ipsum dolor ...
:::

::: note
Lorem ipsum dolor ...
:::

::: tip
Done ✔
:::

::: question
Lorem ipsum dolor ...  
Lorem ipsum dolor ...
:::

::: warning

- Lorem
- ipsum
- dolor

:::

::: important
**Lorem ipsum dolor** sit amet, `consectetur adipiscing` elit.

```
if(args.length < 2) {
	System.out.println("Lorem ipsum dolor sit amet");
}
```

_Nam aliquet libero
quis lectus elementum fermentum._
:::
