::: multicolumns

::: column
Div with column class

- l1
- l2

:::

Or something else. The plugin will encapsulate everything not a div and add `column` class

::: another-class
Some content, `column` class will be added to
:::

:::
