# Set language with first class

This is inline code `const foo = "bar"`{.js} colourized as js. Colourize is done by [Prismjs](https://prismjs.com/).

```js
function f() {
  let foo = 'bar'
}
```

# Highlight specific lines

```{.js line="2,4-5"}
function f() {
  let foo = 'bar'
  foo += "2"
  foo += "5"
  return foo
}
```

# Colourize language with diff

Prefix your language class with `diff-`

```diff-js
function f() {
- let foo = 'bar'
+ let foo = 'bar2'
}
```

# Command line

```{.bash user=myuser host=myhost}
ls .
(out) file
```

# Issues

`file`{.treeview} should be on baseline. [Issue 41](https://gitlab.com/dolmen-public/pandoc/-/issues/41)
