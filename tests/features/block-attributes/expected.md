![Exemple of png image with custom attributes](/data/pandoc.png){#id .customClass style="background-color: #000000"}

![Exemple of svg image with custom attributes](/data/pandoc.svg){width="30%"}

``` {#fenced .myclass}
A fenced code block
```

A [link](#fenced) to the block code
