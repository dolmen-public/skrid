  key        Value                                           Details
  ---------- ---------- --------------------------------------------
  a key      a value                               That's **a test**
  footnote   This[^1]     Let's try to add a footnote inside a table

  : A table

[^1]: This should display nicely
