: A table

| key      | Value      |                                    Details |
| :------- | :--------- | -----------------------------------------: |
| a key    | a value    |                          That's **a test** |
| footnote | This[^ft1] | Let's try to add a footnote inside a table |

[^ft1]: This should display nicely
