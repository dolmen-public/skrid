```yaml
metadata:
  placeholder: value
  multiline: |
    line 1
    line 2
  key:
    foo: '`{"fooValue":"barvalue"}`{.json}'
  bar: barValue
```

This is my `{{placeholder}}`
These are my `{{multiline}}`

This is a file with a `{{key.foo}}`
