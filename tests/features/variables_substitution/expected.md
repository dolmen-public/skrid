``` yaml
metadata:
  placeholder: value
  multiline: |
    line 1
    line 2
  key:
    foo: '`{"fooValue":"barvalue"}`{.json}'
  bar: barValue
```

This is my value These are my one line two line

This is a file with a `{"fooValue":"barvalue"}`{.json}
