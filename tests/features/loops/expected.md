It's possible to loop on metadata variables.

You need to add 2 attributes :

-   for : point to the variables to loop on
-   as : **must start with \$**, it's the reference variables that will be replaced in the loop

If

``` yaml
metadata:
    loopExamples:
      first:
        name: firstExample
      second:
        name: secondExample
```

Looping on `loopExamples`

**firstExample**

**secondExample**
