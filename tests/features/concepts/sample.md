A concept is created with `my label of concept`{#myConcept}. You may specify the class `.Concept`. This will create a link to `::myConcept` with correct label.

This :

- `is a concept`{#myConcept1} , `::myConcept1`
- `is a another concept`{#myConcept2 .Concept}, `::myConcept2`
- `is an inline code with and id, not a concept`{#myConcept3 .txt}, [Link to code](#myConcept3)

And this will display a list of all my concepts

```concepts
my.*
```

You can also define aliases. An alias point to an existing concept with another key and label. Aliases definitions are removed.

`This alias point`{#myAlias alias-for=myConcept1}

`::myAlias` to an existing concept.

You can also add definition to concept

::: {def-for=myConcept}
This is the definition for concept `::myConcept`. It will be rendered as

- a paragraph here
- a dialog when hovering a concept link in html

:::

You may also use some filter to tune your concept usages

* Lowercase : `::myConcept1| lower`
* Uppercase : `::myConcept1| upper `
* Capitalize : `::myConcept1|capital`

You can use metadata variables like `{{ author }}`{#author}. Author is `::author`.

# Headers

Headers may contains concepts. By default, they are parsed as plain, but you may activate formatting with either :

``` yml
metadata:
  formatConceptInHeader: true
```

or locally with a class

```md
# Title with formatted `::foo` {#title2 .formatConcept}
```


# Title with plain `::myConcept1` {#title1}

Uses of this title `::title1`

# Title with formatted `::myConcept1` {#title2 .formatConcept}

Uses of this title `::title2`
