It's possible to include files from another.

Inclusion is done via a CodeBlock with `include` or `includes` class and the path of files to include. Paths may be relative to current file or to the root of files (`/data`). You may also use metadata variables inside `includes`.

If you import a directory, it will look for a `main.md` inside it.

-   pandoc.yml

``` yml
metadata:
  includes:
    pathVariable: sample2.md
```

-   sample2.md

# Second file import

#### Second file

Content from directory/main.md

-   sample3.md

# Final file import

## Finale file
