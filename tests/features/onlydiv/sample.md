::: doc-only
This is visible because

```yaml
metadata:
  onlydiv-doc: true
```

is setup in pandoc.yml
:::

::: {.note .dev-only}
This is visible only when `onlydiv-dev` is configured. This is done automatically in dev mode
:::
