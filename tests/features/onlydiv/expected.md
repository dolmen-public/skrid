::: doc-only
This is visible because

``` yaml
metadata:
  onlydiv-doc: true
```

is setup in pandoc.yml
:::
