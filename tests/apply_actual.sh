#!/usr/bin/env bash
CLEAN_PATTERN=actual
OUTPUT_FILE="${CLEAN_PATTERN}"

NC='\033[0m'

function messageOK() {
    echo -e "\033[0;32m✓ $1${NC}"
}

function messageAction() {
    echo -e "\033[0;36m⚑ $1"
}

function messageEnd() {
    echo -e "${NC}"
}

function messageError() {
    echo -e "\033[0;31m✗ $1${NC}"
}

# Main
for file in $(find /tests/ -name "${OUTPUT_FILE}*" -type f); do
    mv ${file} ${file/actual/expected}
done
