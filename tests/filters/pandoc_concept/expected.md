This is a [bar](#foo){.ConceptLink concept="foo"} and multiple [bars](#foos){.ConceptLink concept="foos"}.

This is a `::missing-concept`{.PandocError error-id="errormarkdown3" error-severity="ERROR" error-message="Missing concept : [missing-concept]"}. This `not a concept` and `this neither`.

-   Also
    -   This is a [bar](#foo){.ConceptLink concept="foo"}.
    -   This is a [concept override](#foobar){.ConceptLink concept="foobar"}.
    -   This not a concept ref `const foo = bar`{#mycode .js}
    -   Neither is `this::code`

# Bar

`bar`{#foo .Concept}, `bars`{#foos .Concept}

# Foobar **test** {#foobar}

`concept override`{#foobar .Concept}

# Title with plain bar {#title}

Uses of this title [Title with plain bar](#title){.ConceptLink concept="title"}

# Title with formatted [bar](#foo){.ConceptLink concept="foo"} {#title2 .formatConcept}

Uses of this title [Title with formatted bar](#title2){.ConceptLink concept="title2"}

# List of concepts

Simple regex

-   [bar](#foo){.ConceptLink concept="foo"}
-   [bars](#foos){.ConceptLink concept="foos"}
-   [concept override](#foobar){.ConceptLink concept="foobar"}

Multiple regex

-   [bar](#foo){.ConceptLink concept="foo"}
-   [Bar](#bar){.ConceptLink concept="bar"}
-   [concept override](#foobar){.ConceptLink concept="foobar"}

# Aliases

This is the use of [This is an alias](#foo){.ConceptLink concept="foo"} that should be pointing to [bar](#foo){.ConceptLink concept="foo"}

# Definition

::: {.ConceptDef def-for="foo"}
This is the definition for concept `foo`. It will be rendered as

    - a paragraph here
    - a dialog when hovering a concept link in html
:::

[Error]{.PandocError error-id="errormarkdown2" error-severity="WARN" error-message="Definition already exist for concept : [foo], Skipping"}

# Modifiers

-   Lowercase : [bar](#foo){.ConceptLink concept="foo"}
-   Uppercase : [BAR](#foo){.ConceptLink concept="foo"}
-   Capitalize : [Bar](#foo){.ConceptLink concept="foo"}

## Lowercase : bar {#lowercase-foo-lower}

## Uppercase : BAR {#uppercase-foo-upper}

## Capitalize : Bar {#capitalize-foocapital}

# Variables (uses pandoc_mustache)

This is a variable concept `FooBar`{#vc .Concept} and its use [FooBar](#vc){.ConceptLink concept="vc"}.
