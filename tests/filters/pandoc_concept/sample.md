This is a `::foo` and multiple `::foos`.

This is a `::missing-concept`. This `not a concept` and `this neither`.

- Also
  - This is a `::foo`.
  - This is a `::foobar`.
  - This not a concept ref `const foo = bar`{#mycode .js}
  - Neither is `this::code`

# Bar

`bar`{#foo}, `bars`{#foos .Concept}

# Foobar **test** {#foobar}

`concept override`{#foobar}

# Title with plain `::foo` {#title}

Uses of this title `::title`

# Title with formatted `::foo` {#title2 .formatConcept}

Uses of this title `::title2`


# List of concepts

Simple regex

```Concepts
foo.*
```

Multiple regex

```concepts
foo
.*bar.*
```

# Aliases

`This is an alias`{#alias alias-for=foo}

This is the use of `::alias` that should be pointing to `::foo`

# Definition

::: {def-for=foo}
This is the definition for concept `foo`. It will be rendered as

    - a paragraph here
    - a dialog when hovering a concept link in html

:::

::: {def-for=foo}
Duplicate definition will be skipped but trigger a warning.
:::

# Modifiers

* Lowercase : `::foo| lower`
* Uppercase : `::foo| upper `
* Capitalize : `::foo|capital`

## Lowercase : `::foo| lower`
## Uppercase : `::foo| upper `
## Capitalize : `::foo|capital`


# Variables (uses pandoc_mustache)

This is a variable concept `{{ concept.label }}`{#vc} and its use `::vc`.
