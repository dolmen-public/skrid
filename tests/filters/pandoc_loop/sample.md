::: { for=my.users as=$user }
User `{{ $user }}` is named `{{ $user.name }}` with [Link]({{ $user.link }})
:::

----

::: { for=my.users as=$user }
::: { if="$user.name is 'me'" }
Me
:::

[me]{ if="$user.name is 'me'" }

:::

----

::: { for=my.users as=$userA }
::: { for=my.users as=$userB }
`{{ $userA }}` `{{ $userB }}`
:::
:::

----

::: { for=my.users as=user }
Missing $ before user
:::

----

::: { for=my.users }
Missing as
:::

----

::: { for=missing.meta as=$meta}
Missing meta
:::
