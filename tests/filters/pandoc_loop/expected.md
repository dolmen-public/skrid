User `{{ my.users.1 }}` is named `{{ my.users.1.name }}` with [Link](%7B%7B%20my.users.1.link%20%7D%7D)

User `{{ my.users.two }}` is named `{{ my.users.two.name }}` with [Link](%7B%7B%20my.users.two.link%20%7D%7D)

------------------------------------------------------------------------

::: {if="my.users.1.name is 'me'"}
Me
:::

[me]{if="my.users.1.name is 'me'"}

::: {if="my.users.two.name is 'me'"}
Me
:::

[me]{if="my.users.two.name is 'me'"}

------------------------------------------------------------------------

`{{ my.users.1 }}` `{{ my.users.1 }}`

`{{ my.users.1 }}` `{{ my.users.two }}`

`{{ my.users.two }}` `{{ my.users.1 }}`

`{{ my.users.two }}` `{{ my.users.two }}`

------------------------------------------------------------------------

::: {.PandocError for="my.users" as="user" error-id="errormarkdown1" error-severity="ERROR" error-message="'user' must begin by '$'"}
Missing \$ before user
:::

------------------------------------------------------------------------

::: {.PandocError for="my.users" error-id="errormarkdown2" error-severity="ERROR" error-message="Missing 'as' attribute"}
Missing as
:::

------------------------------------------------------------------------

::: {.PandocError for="missing.meta" as="$meta" error-id="errormarkdown3" error-severity="ERROR" error-message="Metadata not found : missing.meta"}
Missing meta
:::
