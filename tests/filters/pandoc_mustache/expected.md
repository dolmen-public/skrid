This is my value These are my one line two line

Dot notation variable `{"fooValue":"barvalue"}`{.json}.

Ignore space in curly brackets `{"fooValue":"barvalue"}`{.json}.

And this is `{{notFound}}`{.PandocError error-id="errormarkdown1" error-severity="WARN" error-message="Metadata not found : notFound | Skipping"}

Do not process `{{ some.concept}}`{#myConcept}

It works in a link :

-   in href : [Foo](https://foo.example) or [Foo](https://foo.example)
-   space does not mattter [Foo](https://foo.example) or [Foo](https://foo.example)
-   in label : [My label](https://bar.example)

[LinkNotFound](%7B%7B%20nofFound%20%7D%7D){.PandocError error-id="errormarkdown2" error-severity="WARN" error-message="Metadata not found : nofFound | Skipping"}
