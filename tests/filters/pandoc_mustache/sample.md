This is my `{{placeholder}}`
These are my `{{multiline}}`

Dot notation variable `{{key.foo}}`.

Ignore space in curly brackets `{{ key.foo   }}`.

And this is `{{notFound}}`

Do not process `{{ some.concept}}`{#myConcept}


It works in a link :

* in href : [Foo]({{link.target}}) or [Foo](`{{link.target}}`)
* space does not mattter [Foo](  {{ link.target     }}   ) or [Foo](`{{     link.target}}`)
* in label : [`{{ link.label }}`](https://bar.example)


[LinkNotFound]({{ nofFound }})
