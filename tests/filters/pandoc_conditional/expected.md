::: {if="active"}
included because `active` is defined to any string
:::

::: {if="foo.bar is 'test'"}
included because `foo.bar` is 'test'
:::

::: {if="foo.barSpace is 'te st'"}
included because foo.barSpace equal 'te st'
:::

::: {if="foo.bar not is 'something'"}
included because `foo.bar` is 'test'
:::

::: {if="foo.bar is foo.bar2"}
included because `foo.bar` is 'test' and `foo.bar2` is 'test'
:::

[included because `foo.bar` is 'test']{if="foo.bar is 'test'"}

[included because `foo.bar` is 'test' and `foo.barSpace` is 'te st']{if="foo.bar not is foo.barSpace"}
