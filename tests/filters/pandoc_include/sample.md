# Chapter sample.md

```includes
{{file}}
```

## Section sample.md

```includes
recursion1.md
{{file}}
```

### Title sample.md

## Section sample.md

```{.includes shift-heading-level-by=0}
{{file}}
```

``` {.includes if="active"}
included.md
```

``` {.includes if="inactive"}
excluded.md
```
