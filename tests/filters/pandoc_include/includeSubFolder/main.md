# SubFolder recursion

includeSubFolder/main.md content

```includes
includeWithinCurentFolder.md
includeFromBase.md
includeFromAnotherFolder/main.md
includeWithinCurentFolder.md
```

## Image

![image same folder](pandoc.png)

![image sub folder](sub/pandocSub.png)

![image root relative to another](includeFromAnotherFolder/pandocAnotherSub.png)

![image root relative to same](includeSubFolder/pandoc.png)

![image root relative to sub](includeSubFolder/sub/pandocSub.png)

![Inexistant image](inexistant.png)

![External link image](https://image.example)

## Import directory

``` include
missingDirectory
```

``` include
missingMain
```

``` include
missingMain/
```

``` include
workingDirect
````

``` include
workingDirect/
````

``` include
includeSubFolder/workingDirect
````

``` include
includeSubFolder/workingDirect/
```
