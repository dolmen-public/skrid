# Chapter sample.md

## Chapter

This chapter should be a different heading level

## Section sample.md

Testing recursion

### SubFolder recursion

includeSubFolder/main.md content

includeWithinCurentFolder.md

includeFromBase.md

includeFromAnotherFolder/main.md

includeWithinCurentFolder2.md

![Another image](/tests/filters/pandoc_include/includeFromAnotherFolder/pandocAnotherSub.png)

includeFromAnotherFolder/main.md

includeWithinCurentFolder2.md

![Another image](/tests/filters/pandoc_include/includeFromAnotherFolder/pandocAnotherSub.png)

includeWithinCurentFolder.md

#### Image

![image same folder](/tests/filters/pandoc_include/includeSubFolder/pandoc.png)

![image sub folder](/tests/filters/pandoc_include/includeSubFolder/sub/pandocSub.png)

![image root relative to another](/tests/filters/pandoc_include/includeFromAnotherFolder/pandocAnotherSub.png)

![image root relative to same](/tests/filters/pandoc_include/includeSubFolder/pandoc.png)

![image root relative to sub](/tests/filters/pandoc_include/includeSubFolder/sub/pandocSub.png)

[Inexistant image]{.PandocError error-id="errormarkdown4" error-severity="ERROR" error-message="Image not found : inexistant.png"}

![External link image](https://image.example)

#### Import directory

[Error]{.PandocError error-id="errormarkdown1" error-severity="WARN" error-message="Cannot include file missingDirectory from /tests/filters/pandoc_include/includeSubFolder/missingDirectory or /tests/filters/pandoc_include/missingDirectory"}

[Error]{.PandocError error-id="errormarkdown2" error-severity="WARN" error-message="Cannot include directory missingMain: Missing file main.md"}

[Error]{.PandocError error-id="errormarkdown3" error-severity="WARN" error-message="Cannot include directory missingMain/: Missing file main.md"}

Should be displayed

Should be displayed

Should be displayed

Should be displayed

### Chapter {#chapter}

This chapter should be a different heading level

### Title sample.md

## Section sample.md

# Chapter {#chapter}

This chapter should be a different heading level

Should appears on actual doc
