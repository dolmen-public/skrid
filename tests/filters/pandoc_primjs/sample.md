```js
const foo = 'bar'
```

`const foo = 'bar'`{.js}

```language-php
$toto = "bar"
```

```{.js line="2,4-5"}
function f() {
  let foo = 'bar'
  foo += "2"
  foo += "5"
  return foo
}
```

```diff-js
function f() {
- let foo = 'bar'
+ let foo = 'bar2'
}
```

```{.bash user=myuser}
ls .
```
