Some text

Some text

::: multicolumns
::: column
Some text on first column
:::

::: column
Some text on second column
:::

::: column
Some text on third column
:::
:::

::: multicolumns
::: {.not-column-class .column}
Some text on first column
:::

::: column
Some text out of div but managed by the filter on next column
:::
:::
