::: doc-only
This is visible only if `doc` is present in `onlydiv` metadata
:::

::: {.note .dev-only}
This is visible only if `dev` is present in `onlydiv` metadata
:::
