#!/usr/bin/env bash
CLEAN_PATTERN=actual
OUTPUT_FILE="${CLEAN_PATTERN}"
ERROR_FILE_MD="${CLEAN_PATTERN}_md.error"
ERROR_FILE_TEX="${CLEAN_PATTERN}_tex.error"
ERROR_FILE_HTML="${CLEAN_PATTERN}_html.error"
EXPECTED_COUNT=0
ERRORS_COUNT=0
PASSED_COUNT=0
SKIPPED_COUNT=0
export SPI_DATE=01/01/2020

NC='\033[0m'
REWRITE_PREVIOUS_LINE='\e[1A'

function messageOK() {
  echo -e "\033[0;32m✓ $1${NC}"
}

function messageAction() {
  echo -e "\033[0;36m⚑ $1"
}

function messageEnd() {
  echo -e "${NC}"
}

function messageError() {
  echo -e "\033[0;31m✗ $1${NC}"
}

# $1 expected file path
# $2 actual file path
# $3 type of files
function assertNoDiff() {
  ((EXPECTED_COUNT++))
  local status
  if [ ! -f "${1}" ]; then
    [ ! -s "${2}" ]
    status=$?
    if [ ${status} = 0 ]; then
      ((PASSED_COUNT++))
      rm -f "${2}"
    elif [ ${status} = 1 ]; then
      messageError "  Subpart ${3} | File should be empty"
      messageError "    Actual   [${2}]"
      ((ERRORS_COUNT++))
    fi
  else
    diff "${1}" - <"${2}" 1>/dev/null
    status=$?
    if [ ${status} = 0 ]; then
      ((PASSED_COUNT++))
      rm -f "${2}"
    elif [ ${status} = 1 ]; then
      messageError "  Subpart ${3} | Differences found"
      messageError "    Actual   [${2}]"
      messageError "    Expected [${1}]"
      ((ERRORS_COUNT++))
    else
      messageError "  Subpart ${3} | Failed computing diff"
      messageError "    Actual   [${2}]"
      messageError "    Expected [${1}]"
      ((ERRORS_COUNT++))
    fi
  fi
}

# Main
messageAction "⚒ Testing filters"
for d in /tests/filters/*/; do
  if [ -n "$1" ] && [[ "${d}" != *"$1"* ]]; then
    ((SKIPPED_COUNT++))
    continue
  fi

  # shellcheck disable=SC2164
  cd "${d}"

  messageAction "${d}"
  rm -f "./${CLEAN_PATTERN}"*
  ERROR_AT_START=$ERRORS_COUNT

  if [ -f "${d}expected.md" ]; then
    pandoc -d "${d}pandoc.yml" "${d}/sample.md" -t markdown \
      -o "${d}${OUTPUT_FILE}.md" \
      --wrap=none \
      2>"${d}${ERROR_FILE_MD}"
    assertNoDiff "${d}expected_md.error" "${d}${ERROR_FILE_MD}" "error"
    assertNoDiff "${d}expected.md" "${d}${OUTPUT_FILE}.md" "markdown"
  fi

  if [ -f "${d}expected.tex" ]; then
    pandoc -d "${d}pandoc.yml" "${d}sample.md" -t latex \
      -o "${d}${OUTPUT_FILE}.tex" \
      --wrap=none \
      2>"${d}${ERROR_FILE_TEX}"
    assertNoDiff "${d}expected_tex.error" "${d}${ERROR_FILE_TEX}" "error"
    assertNoDiff "${d}expected.tex" "${d}${OUTPUT_FILE}.tex" "latex"
  fi

  if [ -f "${d}expected.html" ]; then
    pandoc -d "${d}pandoc.yml" "${d}sample.md" -t html \
      --wrap=none \
      -o "${d}${OUTPUT_FILE}.html" \
      --wrap=none \
      2>"${d}${ERROR_FILE_HTML}"
    assertNoDiff "${d}expected_html.error" "${d}${ERROR_FILE_HTML}" "error"
    assertNoDiff "${d}expected.html" "${d}${OUTPUT_FILE}.html" "html"
  fi

  if [ "${ERROR_AT_START}" == "${ERRORS_COUNT}" ]; then
    echo -ne "${REWRITE_PREVIOUS_LINE}"
    messageOK "${d}"
  else
    messageError "  Failed"
  fi
done

echo ""
messageAction "⚒ Testing features"
for d in /tests/features/*/; do
  if [ -n "$1" ] && [[ "${d}" != *"$1"* ]]; then
    ((SKIPPED_COUNT++))
    continue
  fi

  # shellcheck disable=SC2164
  cd "${d}"

  messageAction "${d}"
  rm -f "./${CLEAN_PATTERN}"*
  ERROR_AT_START=$ERRORS_COUNT

  FEATURE=$(basename "${d}")

  TMP_DEFAULTS="${d}/pandoc.md-tmp.yml"
  # shellcheck disable=SC2046
  yq merge -x -a=append $(/.pandoc/scripts/datafile.sh markdown) /tests/features/markdown.yml >"${TMP_DEFAULTS}"
  pandoc -d "${TMP_DEFAULTS}" \
    -M "subtitle=${FEATURE}" \
    -o "${d}${OUTPUT_FILE}.md" \
    2>"${d}${ERROR_FILE_MD}"

  assertNoDiff "${d}expected_md.error" "${d}${ERROR_FILE_MD}" "error"

  TMP_DEFAULTS="${d}/pandoc.html-tmp.yml"
  # shellcheck disable=SC2046
  yq merge -x -a=append $(/.pandoc/scripts/datafile.sh html) /tests/features/html.yml >"${TMP_DEFAULTS}"
  pandoc -d "${TMP_DEFAULTS}" \
    -M "subtitle=${FEATURE}" \
    -o "${d}${OUTPUT_FILE}.html" \
    2>>"${d}${ERROR_FILE_HTML}"
  assertNoDiff "${d}expected_html.error" "${d}${ERROR_FILE_HTML}" "error"
  assertNoDiff "${d}expected.html" "${d}${OUTPUT_FILE}.html" "html"

  TMP_DEFAULTS="${d}/pandoc.pdf-tmp.yml"
  # shellcheck disable=SC2046
  yq merge -x -a=append $(/.pandoc/scripts/datafile.sh pdf) /tests/features/pdf.yml >"${TMP_DEFAULTS}"
  pandoc -d "${TMP_DEFAULTS}" \
    -M "subtitle=${FEATURE}" \
    -o "${d}${OUTPUT_FILE}.tex" \
    2>>"${d}${ERROR_FILE_TEX}"
  assertNoDiff "${d}expected_tex.error" "${d}${ERROR_FILE_TEX}" "error"
  assertNoDiff "${d}expected.tex" "${d}${OUTPUT_FILE}.tex" "latex"

  # Testing intermediate md at the end as we need it to generate html/pdf
  assertNoDiff "${d}expected.md" "${d}${OUTPUT_FILE}.md" "markdown"

  if [ "${ERROR_AT_START}" == "${ERRORS_COUNT}" ]; then
    echo -ne "${REWRITE_PREVIOUS_LINE}"
    messageOK "${d}"
    rm -f pandoc.*-tmp.yml
  else
    messageError "  Failed"
  fi
done

echo ""
echo "⚖ Summary"
messageOK "${PASSED_COUNT} passed on ${EXPECTED_COUNT}"
[ "${ERRORS_COUNT}" = 0 ] || messageError "${ERRORS_COUNT} errors"
[ "${SKIPPED_COUNT}" = 0 ] || echo "⚠ ${SKIPPED_COUNT} tests skipped"
exit "${ERRORS_COUNT}"
