import { init } from './changeDocument'
import './style.scss'

console.log('Registering auto-reload.')

const footer = document.createElement('footer')
document.querySelector('body')!.appendChild(footer)

const icon = document.createElement('i')
footer.appendChild(icon)
icon.classList.add('Reload')
icon.title = 'Connected to liveServer and listening to changes.'

let timeout = -1

function reload() {
  if (timeout) {
    window.clearTimeout(timeout)
  }
  icon.classList.add('scheduled')
  timeout = window.setTimeout(() => {
    location.reload()
    icon.classList.remove('scheduled')
  }, 1000)
}

const protocol = location.protocol === 'http:' ? 'ws' : 'wss'
const url = `${protocol}://${location.host}`
console.log(`Autoreload address ${url}`)
const ws = new WebSocket(url)

ws.onmessage = function (event) {
  const [dir, reason, file] = event.data.split(' ')
  console.log('Change detected', { dir, reason, file })
  reload()
}

init(footer, ws)
