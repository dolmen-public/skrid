export function init(footer: HTMLElement, ws: WebSocket) {
  if (!availableDocs || !availableDocs.length) return

  const dropDown = document.createElement('select')
  footer.appendChild(dropDown)

  console.log('curr', currentDoc)

  for (const doc of availableDocs) {
    const option = document.createElement('option')
    option.value = doc
    option.text = doc
    option.selected = doc === currentDoc
    dropDown.append(option)
  }

  footer.onchange = (e: Event) => {
    const doc = (e?.target as HTMLSelectElement)?.selectedOptions[0].value
    ws.send(doc)
  }
}
