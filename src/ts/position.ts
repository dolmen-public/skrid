import * as Nav from './nav'

const article = document.querySelector('article')!

export async function init() {
  let options = {
    rootMargin: '0% 0% -95% 0%',
    threshold: 0,
  }
  let observer = new IntersectionObserver(observed, options)
  article.querySelectorAll('h1, h2, h3, h4, h5, h6').forEach((heading) => {
    if (heading.id) observer.observe(heading)
  })
}

const observed: IntersectionObserverCallback = function (entries) {
  entries
    .filter((entry) => entry.isIntersecting)
    .forEach((e) => {
      let ref = '#' + e.target.id
      history.replaceState('', '', window.location.pathname + ref)
      Nav.update(ref)
    })
}
