function createSummary(logs: string[]) {
  let summary = document.createElement('summary')
  summary.classList.add('PandocError__Summary')
  summary.textContent = `${logs.length} errors found.`

  let nextButton = document.createElement('button')
  nextButton.textContent = 'Go to first error'
  nextButton.classList.add('PandocError__First')
  nextButton.onclick = () => {
    document.querySelector('.PandocError')?.scrollIntoView()
  }

  summary.append(nextButton)

  return summary
}

function createHeader(logs: string[]) {
  let headerError = document.createElement('details')
  headerError.classList.add('PandocError__Message')
  headerError.append(createSummary(logs))

  let errorList = document.createElement('ul')
  errorList.classList.add('PandocError__List')
  headerError.append(errorList)

  for (const log of logs) {
    let errorItem = document.createElement('li')
    let errorLink = document.createElement('a')
    errorLink.classList.add('PandocError__Link')
    try {
      let
        error = JSON.parse(log)
      errorLink.textContent = error.message
      errorLink.href = `/#${error.id}`
    } catch (e) {
      errorLink.textContent = log
    }
    errorItem.append(errorLink)
    errorList.append(errorItem)
  }

  return headerError
}

export async function init() {
  insertErrorMarker()

  const logFileLink = logfile.replace('/public', './')

  let logResponse = await fetch(logFileLink, {method: 'GET'})
  if (logResponse.status === 200) {
    let text = await logResponse.text()
    let logs = text.split('\n').filter((str) => str)
    if (logs.length) {
      document.body.prepend(createHeader(logs))
    }
  }
}

function insertErrorMarker() {
  let elementsWithError = document.querySelectorAll('.PandocError')

  for (const element of elementsWithError) {
    let errorAnchor = document.createElement('span')
    errorAnchor.classList.add('PandocError__Anchor')
    let errorMessage =
      element.attributes.getNamedItem('data-error-message')?.value
    if (errorMessage) errorAnchor.title = errorMessage
    let errorID = element.attributes.getNamedItem('data-error-id')?.value
    if (errorID) errorAnchor.id = errorID
    element.parentElement?.insertBefore(errorAnchor, element)
  }
}
