import {cumulativeOffset} from "./cumulativeOffset";

const article = document.querySelector('article')!
let createTimeout: number = 0
let removeTimeout: number = 0

const cancel = {
  create: () => {
    clearTimeout(createTimeout)
    createTimeout = 0
  },
  remove: () => {
    clearTimeout(removeTimeout)
    removeTimeout = 0
  },
}

const Concept = {
  getIdFromEvent: (event: Event): string | null =>
    (event.target as Element).getAttribute('data-concept'),
  definition: {
    class: 'ConceptDef',
    get: (id: string) =>
      document.querySelector(Concept.definition.selector(id)),
    selector: (id: string) =>
      ['.', Concept.definition.class, '[data-def-for="', id, '"]'].join(''),
  },
  popin: {
    class: 'ConceptPopin',
    selector: (id: string | null = null) => {
      let selectors = ['.', Concept.popin.class]
      if (id) selectors.push('[data-def-for="', id, '"]')
      return selectors.join('')
    },
  },
}

function syncCreate(event: Event) {
  syncRemove()
  let id = Concept.getIdFromEvent(event)
  if (!id) return
  let originalDef = Concept.definition.get(id)
  if (originalDef) {
    let popIn = originalDef.cloneNode(true) as HTMLElement
    popIn.classList.remove(Concept.definition.class)
    popIn.classList.add(Concept.popin.class)

    popIn.addEventListener('focus', cancel.remove)
    popIn.addEventListener('mouseenter', cancel.remove)
    popIn.addEventListener('mouseleave', () => remove(id))
    popIn.addEventListener('blur', () => remove(id))

    let target = event.target
    article.appendChild(popIn)
    if (target instanceof HTMLAnchorElement) {
      let coord = cumulativeOffset(target, article)

      if (target.getBoundingClientRect().top > window.innerHeight / 2) {
        //unfold abov link
        popIn.style.bottom = article.offsetHeight - coord.top + 'px'
      } else {
        //unfold below link
        popIn.style.top = coord.top + target.offsetHeight + 'px'
      }

      let left: number
      if (event instanceof MouseEvent) {
        let coordArticle = cumulativeOffset(article)
        left = event.clientX - coordArticle.left
      } else {
        left = coord.left + target.offsetWidth / 2
      }
      popIn.style.left =
        Math.max(
          0,
          Math.min(
            article.offsetWidth - popIn.offsetWidth,
            left - popIn.offsetWidth / 2
          )
        ) + 'px'
    }
  }
}

function syncRemove(conceptId: string | null = null) {
  article
    .querySelectorAll(Concept.popin.selector(conceptId))
    .forEach((p) => p.remove())
}

function remove(conceptId: string | null = null) {
  cancel.create()
  if (removeTimeout) return
  removeTimeout = window.setTimeout(() => {
    syncRemove(conceptId)
    removeTimeout = 0
  }, 400)
}

function create(event: Event) {
  if (createTimeout) return
  createTimeout = window.setTimeout(() => {
    syncCreate(event)
    createTimeout = 0
  }, 200)
}

export async function init() {
  document.querySelectorAll('.ConceptLink').forEach((link) => {
    link.addEventListener('focus', create)
    link.addEventListener('mouseenter', create)
    link.addEventListener('mouseleave', (e: Event) =>
      remove(Concept.getIdFromEvent(e))
    )
    link.addEventListener('blur', (e: Event) =>
      remove(Concept.getIdFromEvent(e))
    )
  })
}
