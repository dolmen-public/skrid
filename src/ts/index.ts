import '../style/all.scss'
import * as Header from './header'
import * as FootNote from './footnote'
import * as Nav from './nav'
import * as Position from './position'
import * as Prism from './prism'
import * as Concept from './concept'
import * as Error from './error'

require('typeface-montserrat')

declare global {
  /**
   * Filename of PDF version
   * Injected in html template
   * {@link file://../../pandoc/pandoc/templates/report.html}
   */
  const pdfVersion: string

  /**
   * Filename of html log
   * Injected in html template
   * {@link file://../../pandoc/pandoc/templates/report.html}
   */
  const logfile: string

  /**
   * Possible documents
   */
  const availableDocs: string[]

  /**
   * In case of multiple document, the current one
   */
  const currentDoc: string
}

document.addEventListener('DOMContentLoaded', () => {
  window.onhashchange = () => {
    const element = document.querySelector(
      decodeURIComponent(window.location.hash)
    )
    if (element) {
      element.scrollIntoView()
    }
  }

  Header.init()
  Error.init()
  FootNote.init()
  Concept.init()
  Nav.init()
  Position.init()
  Prism.init()
})
