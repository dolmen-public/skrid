
export function cumulativeOffset(element: HTMLElement, stop: Element | null = null) {
  let total = {
    top: 0,
    left: 0,
  }
  do {
    total.top += element.offsetTop || 0
    total.left += element.offsetLeft || 0
    element = element.offsetParent as HTMLElement
  } while (element && element != stop)

  return total
}
