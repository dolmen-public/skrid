const nav = document.querySelector('nav')
let links: NodeListOf<HTMLAnchorElement>

const NAV_CLASS = 'current'
const FOLD_CLASS = 'folded'
const AUTO_UNFOLD = 'autoUnfold'

function foldChildren(node: HTMLElement, fold: boolean) {
  node
    .querySelectorAll('li')
    .forEach((li: HTMLLIElement) => li.classList.toggle(FOLD_CLASS, fold))
}

export async function init() {
  if (!nav) return
  links = nav.querySelectorAll('a')

  nav.querySelectorAll('li').forEach((li) => {
    if (li.getElementsByTagName('ul').length) {
      li.classList.add('folded')
      let button = document.createElement('button')
      button.classList.add('fold')
      button.onclick = () => {
        let folded = li.classList.toggle(FOLD_CLASS)
        if (folded) foldChildren(li, true)
      }
      li.prepend(button)
    }
  })

  let foldControlBar = document.createElement('div')
  foldControlBar.classList.add('foldControlBar')

  let unfold = document.createElement('button')
  unfold.classList.add('unfold')
  unfold.onclick = () => foldChildren(nav, false)
  foldControlBar.appendChild(unfold)

  let fold = document.createElement('button')
  fold.classList.add('fold')
  fold.onclick = () => foldChildren(nav, true)
  foldControlBar.appendChild(fold)

  nav.prepend(foldControlBar)
}

export function update(ref: string | null = null) {
  if (!nav || !links) return
  ref = ref || decodeURIComponent(location.hash)
  if (!ref) return

  nav
    .querySelectorAll('a.' + NAV_CLASS)
    .forEach((e) => e.classList.remove(NAV_CLASS))
  nav
    .querySelectorAll('li.' + AUTO_UNFOLD)
    .forEach((li) => li.classList.remove(AUTO_UNFOLD))

  let link = nav.querySelector('a[href="' + ref + '"]')
  if (link) {
    link.classList.add(NAV_CLASS)
    getLiParents(link.parentElement).forEach((li: HTMLElement) =>
      li.classList.add(AUTO_UNFOLD)
    )
    link.parentElement!.classList.add(AUTO_UNFOLD)
    link.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
      inline: 'start',
    })
  } else {
    let target = document.querySelector<HTMLElement>(ref)
    if (target) {
      let higher = getHigherHeading(target)
      if (higher) update('#' + higher.id)
    }
  }
}

function getLiParents(element: HTMLElement | null) {
  let parents: HTMLElement[] = []
  if (element) {
    let parent = element.parentElement
    if (parent) {
      if (parent.nodeName === 'LI') {
        parents.push(parent)
        parents.push(...getLiParents(parent))
      }
      if (parent.nodeName === 'UL') parents.push(...getLiParents(parent))
    }
  }
  return parents
}

function getHigherHeading(element: HTMLElement) {
  let tag = element.tagName
  const currentLevel = tag.startsWith('h') ? parseInt(tag.substring(1, 2)) : 7

  let sibling = element.previousElementSibling
  while (sibling) {
    let sibTag = sibling.tagName
    if (
      sibTag.startsWith('H') &&
      parseInt(sibTag.substring(1, 2)) < currentLevel
    )
      return sibling
    sibling = sibling.previousElementSibling
  }
}

export function search(pattern: string | null) {
  if (!nav) return
  nav.classList.remove('none-found')
  if (pattern) {
    nav.classList.add('searching')
    let found = false
    links.forEach((link) => {
      let include = link.innerText.toLowerCase().includes(pattern.toLowerCase())
      link.classList.toggle('found', include)
      found = found || include
    })

    if (!found) {
      nav.classList.add('none-found')
    }
  } else {
    nav.classList.remove('searching')
  }
}
