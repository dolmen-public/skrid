import * as Prism from 'prismjs'
export async function init() {
  let body = document.querySelector('body')!
  body.classList.add('line-numbers')
  body.classList.add('match-braces')

  Prism.highlightAll()
}
