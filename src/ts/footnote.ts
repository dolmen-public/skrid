import { cumulativeOffset } from './cumulativeOffset'

const article = document.querySelector('article')!
let createTimeout: number = 0
let removeTimeout: number = 0

const cancel = {
  create: () => {
    clearTimeout(createTimeout)
    createTimeout = 0
  },
  remove: () => {
    clearTimeout(removeTimeout)
    removeTimeout = 0
  },
}

type CreateInput = {
  id: string
  footnoteRef: HTMLElement
  event: FocusEvent | MouseEvent
}

class FootnoteManager {
  readonly class = 'FootnotePopin'

  getPopinId(id: string) {
    return `popin${id}`
  }

  selector(id: string | null = null) {
    if (id) return `#${this.getPopinId(id)}`
    return ['.', this.class].join('')
  }

  syncCreate(input: CreateInput) {
    this.syncRemove()
    let originalDef = document.getElementById(input.id)
    if (originalDef) {
      let popIn = originalDef.cloneNode(true) as HTMLElement
      popIn.classList.add(this.class)
      popIn.id = this.getPopinId(input.id)

      popIn.addEventListener('focus', cancel.remove)
      popIn.addEventListener('mouseenter', cancel.remove)
      popIn.addEventListener('mouseleave', () => this.remove(input.id))
      popIn.addEventListener('blur', () => this.remove(input.id))

      popIn.querySelectorAll('.footnote-back').forEach((p) => p.remove())

      article.appendChild(popIn)

      const coordinates = cumulativeOffset(input.footnoteRef, article)

      if (
        input.footnoteRef.getBoundingClientRect().top >
        window.innerHeight / 2
      ) {
        //unfold abov link
        popIn.style.bottom = article.offsetHeight - coordinates.top + 'px'
      } else {
        //unfold below link
        popIn.style.top =
          coordinates.top + input.footnoteRef.offsetHeight + 'px'
      }

      let left: number
      if (input.event instanceof MouseEvent) {
        const articleCoordinate = cumulativeOffset(article)
        left = input.event.clientX - articleCoordinate.left
      } else {
        left = coordinates.left + input.footnoteRef.offsetWidth / 2
      }
      popIn.style.left =
        Math.max(
          0,
          Math.min(
            article.offsetWidth - popIn.offsetWidth,
            left - popIn.offsetWidth / 2
          )
        ) + 'px'
    }
  }

  syncRemove(id: string | null = null) {
    article.querySelectorAll(this.selector(id)).forEach((p) => p.remove())
  }

  remove(id: string | null = null) {
    cancel.create()
    if (removeTimeout) return
    removeTimeout = window.setTimeout(() => {
      this.syncRemove(id)
      removeTimeout = 0
    }, 400)
  }

  create(input: CreateInput) {
    if (createTimeout) return
    createTimeout = window.setTimeout(() => {
      this.syncCreate(input)
      createTimeout = 0
    }, 200)
  }
}

export async function init() {
  const footnoteManager = new FootnoteManager()
  document
    .querySelectorAll('article .footnote-ref')
    .forEach((footnoteRef: Element) => {
      let footnote = document.getElementById(
        footnoteRef.getAttribute('href')!.substring(1)
      )

      if (footnote && footnoteRef instanceof HTMLElement) {
        const id = footnote.id
        footnoteRef.addEventListener('focus', (event: FocusEvent) =>
          footnoteManager.create({ id, footnoteRef, event })
        )
        footnoteRef.addEventListener('mouseenter', (event: MouseEvent) =>
          footnoteManager.create({ id, footnoteRef, event })
        )
      }
    })
}
