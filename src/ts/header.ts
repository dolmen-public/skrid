import * as Nav from './nav'

const CLASS_MENU_OPENED = 'display'

const header = document.querySelector('header')
const nav = document.querySelector('nav')
const body = document.querySelector('body')

function toggleMenu(force?: boolean) {
  force = header!.classList.toggle(CLASS_MENU_OPENED, force)
  nav!.classList.toggle(CLASS_MENU_OPENED, force)
  body!.classList.toggle(CLASS_MENU_OPENED, force)
}

const searchComp = document.createElement('span')

const buttonMenu = document.createElement('button')
buttonMenu.innerHTML =
  '<svg viewBox="0 0 24 24"><path d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z"></path></svg>'
buttonMenu.classList.add('ButtonMenu')
buttonMenu.title = 'Open menu'
buttonMenu.addEventListener('click', () => toggleMenu())
document.querySelector('body')!.append(buttonMenu)

export async function init() {
  checkForPdf().catch()

  let width = window.innerWidth > 0 ? window.innerWidth : screen.width

  toggleMenu(width >= 600)

  searchComp.classList.add('search')
  header!.append(searchComp)

  let input = document.createElement('input')
  input.setAttribute('placeholder', 'Search...')
  input.type = 'text'
  searchComp.append(input)

  let clear = document.createElement('button')
  clear.onclick = () => {
    input.value = ''
    search()
  }
  searchComp.append(clear)

  input.addEventListener('input', (e: Event) => {
    let event = e as InputEvent
    if (event.target) search((event.target as HTMLInputElement).value)
  })
}

function search(pattern: string | null = null) {
  Nav.search(pattern)
  searchComp.classList.toggle('searching', !!pattern)
}

async function checkForPdf() {
  const pdfLink = pdfVersion.replace('/public', './')

  let pdf = await fetch(pdfLink, { method: 'HEAD' })
  if (pdf.ok) {
    let printButton = document.createElement('a')
    printButton.href = pdfLink
    printButton.id = 'printButton'
    searchComp.appendChild(printButton)
  }
}
