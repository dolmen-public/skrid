# ------------
# Pandoc image
# See https://github.com/pandoc/dockerfiles
# ------------
FROM pandoc/latex:2 as pandoc

RUN apk add --update coreutils rsync && rm -rf /var/cache/apk/*

ENV MUSL_LOCALE_DEPS cmake make musl-dev gcc gettext-dev libintl
ENV MUSL_LOCPATH /usr/share/i18n/locales/musl

RUN apk add --no-cache \
    $MUSL_LOCALE_DEPS \
    && wget https://gitlab.com/rilian-la-te/musl-locales/-/archive/master/musl-locales-master.zip \
    && unzip musl-locales-master.zip \
    && cd musl-locales-master \
    && cmake -DLOCALE_PROFILE=OFF -D CMAKE_INSTALL_PREFIX:PATH=/usr . && make && make install \
    && cd .. && rm -r musl-locales-master

# LANG is overriden at run time
ENV LANG=en_US.UTF-8

RUN apk add --no-cache --update \
    bash \
    inotify-tools \
    ttf-liberation \
    ttf-dejavu \
    lua-filesystem \
    && fc-cache -fv

RUN tlmgr install \
    adjustbox \
    background \
    collectbox \
    everypage \
    enumitem \
    environ \
    float \
    fontawesome5 \
    footmisc \
    footnotebackref \
    fvextra \
    koma-script \
    lineno \
    lastpage \
    listingsutf8 \
    ly1 \
    mdframed \
    mweights \
    needspace \
    realscripts \
    pdfpages \
    pagecolor \
    sectsty \
    sourcecodepro \
    sourcesanspro \
    svg \
    tcolorbox \
    titlesec \
    titling \
    transparent \
    trimspaces \
    xltxtra \
    zref

ARG YQ_VERSION=3.4.0
RUN wget https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 -O /usr/bin/yq \
    && chmod +x /usr/bin/yq

# https://github.com/joewalnes/websocketd
RUN wget https://github.com/joewalnes/websocketd/releases/download/v0.4.1/websocketd-0.4.1-linux_amd64.zip -O websocketd.zip \
    && mkdir /usr/local/lib/websocketd \
    && unzip websocketd.zip -d /usr/local/lib/websocketd \
    && ln -s /usr/local/lib/websocketd/websocketd /usr/local/bin/ \
    && rm -f websocketd.zip

# Compatibility for libc binary, see https://stackoverflow.com/questions/34729748/installed-go-binary-not-found-in-path-on-alpine-linux-docker
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

ENV WORKDIR=/tmp/workdir \
    SPI_WATCH=false \
    WATCH_SKIP_START=false \
    SPI_FORMAT=all \
    SPI_DEV=false \
    SPI_DATE='' \
    SPI_DATE_ARG="+%_d %B %Y" \
    SPI_TMP_DIR=/tmp \
    NOTIFICATIONS=/tmp/inotify_events \
    WEBSOCKETD_LOGLEVEL=fatal \
    PANDOC_DATA=/.pandoc \
    CONFIG_PATH=/config \
    REPORT_FILES="${PANDOC_DATA}/report-files/" \
    MULTIPLE_TARGET_CURRENT_DIR=/multiple_target/ \
    MULTIPLE_TARGET=false


COPY pandoc ${PANDOC_DATA}
RUN ln -s ${PANDOC_DATA} /root/.pandoc && \
    mkdir -p /public && \
    mkdir -p ${CONFIG_PATH} && \
    mkdir -p ${REPORT_FILES} && \
    mkdir -p ${WORKDIR} \
    mkdir -p ${MULTIPLE_TARGET_CURRENT_DIR}

COPY pandoc/filters/modules /usr/share/lua/common/modules

COPY config ${CONFIG_PATH}
COPY docs /data
COPY tests /tests
COPY ./public ${REPORT_FILES}

# Allow to run with any user
RUN chmod -R 777 \
    ${CONFIG_PATH} \
    ${PANDOC_DATA} \
    ${REPORT_FILES} \
    /public \
    /data \
    /${WORKDIR} \
    ${MULTIPLE_TARGET_CURRENT_DIR}

WORKDIR ${WORKDIR}

EXPOSE 8080

COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
CMD []

