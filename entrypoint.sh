#!/usr/bin/env bash
set -e
set -u

sigterm_handler() {
  echo "sigterm handler called..."
  exit 0;
}

trap 'kill ${!}; sigterm_handler' TERM


if [[ $# -ne 0 ]]; then
  exec "$@"
elif [[ "${SPI_WATCH}" = true ]]; then
  . /.pandoc/scripts/watch.sh

  if [[ "${WATCH_SKIP_START}" != 'true' ]]; then
    /.pandoc/scripts/run.sh
  fi

  while true; do
    if [ -s "${NOTIFICATIONS}" ]; then
      notifies=$(cat "${NOTIFICATIONS}")
      true >"${NOTIFICATIONS}"
      echo "Entrypoint | ${notifies}"
      /.pandoc/scripts/run.sh
    else sleep "${WATCHER_DELAY}"; fi
  done
else
  /.pandoc/scripts/run.sh
fi
