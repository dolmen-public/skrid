#!/usr/bin/env bash

echo '```yaml' >"${WORKDIR}"/default-watcher.yml.md
cat "${PANDOC_DATA}"/defaults/watcher.yml >>"${WORKDIR}"/default-watcher.yml.md
echo '```' >>"${WORKDIR}"/default-watcher.yml.md
