#!/usr/bin/env bash
TARGET=${WORKDIR}/tests.md

if [ ! -f "${TARGET}" ]; then
  touch "${TARGET}"
  chmod 777 "${TARGET}"
fi

{
  echo '# Exemples'
  echo ''
  echo 'From here are aggregated all features tests which are good exemple of usage.'
  echo ''
  echo '`pandoc.yml`{.treeview} for this document:'
  echo ''
  echo '```yaml'
  cat "pandoc.yml"
  echo '```'
} >"${TARGET}"

for d in /tests/features/*/; do
  {
    echo '## '$(basename ${d} | sed 's/_/ /g')"{#$(basename ${d})}"
    echo ''
    echo '### Source'
    echo ''
    echo '`````````````````````markdown'
    cat "${d}sample.md"
    echo '`````````````````````'
    echo ''
    echo '### Rendered as'
    echo ''
    echo '``` includes'
    echo "${d}sample.md"
    echo '```'
    echo ''
  } >>"${TARGET}"
done
