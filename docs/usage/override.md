# Overriding configuration

Data files are read first in /data directory then in /.pandoc/defaults.
This is the priority order of data files, the latest override the first:

: Override order of data files

| File                                        | Location search   | Mandatory | Default location  |
|:--------------------------------------------| :---------------- | :-------- | :---------------- |
| `all.yml`{.treeview}                        | /.pandoc/defaults | yes       | /.pandoc/defaults |
| `<html,pdf,markdown,docx>.yml`{.treeview}   | /.pandoc/defaults | yes       | /.pandoc/defaults |
| `*.all.yml`{.treeview}                      | \${CONFIG_PATH}   | no        |                   |
| `*.<html,pdf,markdown,docx>.yml`{.treeview} | \${CONFIG_PATH}   | no        |                   |
| `pandoc.yml`{.treeview}                     | /data             | yes       |                   |
| `*.all.yml`{.treeview}                      | /data             | no        |                   |
| `*.<html,pdf,markdown,docx>.yml`{.treeview} | /data             | no        |                   |

## Override docx styles

`docx` styles are imported by default from `/.pandoc/defaults/reference.docx`

You may use your own `reference.docx` by overriding `reference-doc` in `pandoc.yml` or in `something.docx.yml`
