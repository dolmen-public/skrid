# LiveServer : Webserver with autoreload {#lv-title}

`LiveServer`{#live-server alias-for=lv-title}

::: {def-for=lv-title}
If watch mode is enabled (ie : `SPI_WATCH: true`{.yaml}) :

- watcher rebuild doc when changes is detected
- web server is deployed for html doc on port 8080
- html doc auto-reload when regenerated

:::

## Configuration

`::live-server` may be configured with a `watcher.yml` file in project to override the default values :

```includes
default-watcher.yml.md
```

::: warning
The arrays, like `directories:`{.yaml} are appended, not overwritten.
:::
