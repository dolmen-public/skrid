# Automatic mode {#auto-mode-title}

::: tip
This is the recommended way. It triggers when no arguments are provided to command line.
:::

```treeview
<your-project>
├── docs
|   ├── *.md  # Documentation splitted files, includes by main.md
|   ├── main.md  # Main entry for documentation, auto loaded
|   └── pandoc.yml
├── docker-compose.yml
└── Makefile
```

Example with docker compose

- docker-compose.yml

  ```yaml
  version: '3.7'

  services:
    pandoc:
      image: registry.gitlab.com/dolmen-public/skrid
      user: ${UID:-0}:${GID:-0}
      volumes:
        - './docs:/data'
        - './public:/public'
      ports:
        - '8080:8080'
  ```

- Makefile

  ```Makefile
  UID=$(shell id -u)
  export UID
  GID=$(shell id -g)
  export GID

  run:
    @docker-compose run --rm pandoc
  ```

- Then run

  ```{.bash .command-line user="user" }
  make run
  ```

- Profit !
