# Run commands before or after generation

You may either use scripts in your document data

```treeview
docs
├── *.before.sh
└── *.after.sh
```

The order will be :

1. `*.before.sh`
2. Pandoc commands
3. `*.after.sh`

::: tip
If `::live-server` is activated, commands will be executed at every runs.
:::
