# Thanks

- [Pandoc Docker](https://github.com/pandoc/dockerfiles) for Pandoc on docker
- [eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template) for a beautiful pdf templates
- [Pandoc filters](https://github.com/pandoc/lua-filters) for usefull filters
