# Environment variables

: Docker image environment variables

| Variables            | Values                             | Default       | Effect                                                                              |
|:---------------------|:-----------------------------------|:--------------|:------------------------------------------------------------------------------------|
| SPI_DATE             | date string                        | ''            | Force date value (do not apply date pattern)                                        |
| SPI_DATE_ARG         | argument for date                  | "+%\_d %B %Y" | Control how date is retrieved                                                       |
| `::spi-format-title` | all / markdown / html / pdf / docx | all           | Activate `::auto-mode-title` and specify format                                     |
| SPI_TMP_DIR          | path                               | /tmp          | Directory for aggregated configuration files                                        |
| SPI_WATCH            | true / false                       | false         | Enables watch mode, dev block (add metadata `onlydiv-dev:true`) and `::live-server` |
| MULTIPLE_TARGET      | doc folder name                    | false         | Enables `::multiple-mode-title`                                                     |                                                                       |
| WATCH_SKIP_START     | true / false                       | false         | If true, disable first run and wait for change                                      |                                                                       |


## SPI_FORMAT {#spi-format-title}

If `SPI_FORMAT=all`, all available output formats are generated. For any others, only the requested format and the necessary intermediates one are generated

* `all` : Generates all available formats
* `markdown` : `.md`
* `html` : `.md` + `.html`
* `pdf`: `.md` + `.latex` + `.pdf`
* `docx`: `.md` + `.docx`
