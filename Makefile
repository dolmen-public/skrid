#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=${PROJECT_NAME}.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

# command name that are also directories
.PHONY: dev

#-----------------------------------------
# Allow passing arguments to make
#-----------------------------------------
SUPPORTED_COMMANDS := yarn test
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
	COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(COMMAND_ARGS):;@:)
endif

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY: help
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


#-----------------------------------------
# Commands
#-----------------------------------------
clean: ## Cleans up environnement
	@mkdir -p ./public
	@rm -rf ./public/*
	@docker-compose down --remove-orphans

dev: clean ## Starts dev stack
	@echo "\nConnect to https://${PROJECT_URL}\n"
	@docker-compose up --build

build: clean ## Build image
	@docker-compose build --pull pandoc

pull: ## Retrieves latest image
	@docker-compose pull

run: ## Generates docs
	@docker-compose run --rm -e SPI_WATCH=false -e SPI_FORMAT=all pandoc

sh:
	@docker-compose run --rm pandoc sh

test: ## Run tests, first args limit test to matching name like *<arg>*
	@docker-compose run --rm --entrypoint=/tests/run.sh pandoc ${COMMAND_ARGS}

apply_actual: ## Apply actual output as expected, Warning : check git diff with care
	@docker-compose run --rm --entrypoint=/tests/apply_actual.sh pandoc

yarn: ## Run yarn <anything> on report webpack docker
	@docker-compose run --rm report yarn ${COMMAND_ARGS}

# --------
# Specific
# --------

update.filters: ## Update pandoc filters from https://github.com/pandoc/lua-filters/
	@bash "${PROJECT_PATH}/pandoc/filters/update-filters.sh"

update.template: ## Updates Eisvogel templates
	@bash "${PROJECT_PATH}/pandoc/templates/update-eisvogel.sh"

dev.multiple: clean ## Starts dev stack for mulitple docs
	@docker-compose -f docker-compose.yml -f docker-compose.multiple.yml up --build
