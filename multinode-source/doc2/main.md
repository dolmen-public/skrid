``` include
common/content.md
```

This is specific to document 2.


![image relative to current folder](pandoc.png){width="100px" style="background-color:#000;"}

![image relative to sub folder](subDoc2/pandoc2.png){width="100px" style="background-color:#000;"}

![image relative to /data to another folder](common/pandocCommon.png){width=100px style="background-color:#000;"}

![image relative to /data to same folder](doc2/pandoc.png){width=100px style="background-color:#000;"}

![image relative to /data to sub folder](doc2/subDoc2/pandoc2.png){width=100px style="background-color:#000;"}
