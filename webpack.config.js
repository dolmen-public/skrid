const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const OUTPUT_ROOT = 'public'
const devMode = process.env.NODE_ENV === 'development'
const babelLoader = {
  loader: 'babel-loader',
  options: {
    presets: ['@babel/preset-env'],
    plugins: ['@babel/plugin-transform-runtime'],
  },
}

module.exports = {
  entry: {
    report: './src/ts/index.ts',
    reload: './src/reload/index.ts',
  },
  output: { filename: '[name].js', path: path.resolve(__dirname, OUTPUT_ROOT), publicPath: '' },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [babelLoader, { loader: 'ts-loader' }],
        exclude: /(node_modules)/,
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: [babelLoader],
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: devMode,
            },
          },
          'css-loader',
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              // Prefer `dart-sass`
              implementation: require('sass'),
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: { outputPath: 'images' },
          },
          { loader: 'image-webpack-loader' },
        ],
      },
      {
        test: /\.(woff|woff2|ttf|otf|eot)$/,
        use: [
          {
            loader: 'file-loader',
            options: { outputPath: 'fonts' },
          },
        ],
      },
      {
        test: /\.(html)$/,
        use: ['html-loader'],
      },
    ],
  },
  devtool: devMode ? 'inline-source-map' : false,
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name].css',
    }),
    new CopyWebpackPlugin({
      patterns: [{ from: 'node_modules/prismjs/components', to: 'components' }],
    }),
  ],
}
